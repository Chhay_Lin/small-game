import React from "react";
import { View, StyleSheet, Text } from "react-native";
import Colors from "../constants/Colors";
const Header = (props) => {
  return (
    <View style={styles.header}>
      <Text style={styles.headerTitle}>{props.title}</Text>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: 90,
    paddingTop: 30,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.primary,
  },
  headerTitle: {
    color: "black",
    fontSize: 18,
  },
});
